 jwt.options = {
      name: 'jwt', // the name to use when invoking the authentication Strategy
      entity: 'user', // the entity that you pull from if an 'id' is present in the payload
      service: 'users', // the service to look up the entity
      passReqToCallback: true, // whether the request object should be passed to `verify`
      jwtFromRequest: [ // a passport-jwt option determining where to parse the JWT
        jwt.ExtractJwt.fromHeader, // From "Authorization" header
        jwt.ExtractJwt.fromAuthHeaderWithScheme('Bearer'), // Allowing "Bearer" prefix
        jwt.ExtractJwt.fromBodyField('body') // from request body
      ],
      secretOrKey: '***', // Your main secret provided to passport-jwt
      session: false, // whether to use sessions,
      Verifier: jwt.Verifier // A Verifier class. Defaults to the built-in one but can be a custom one. See below for details.
  }    

 /*
  const authLocalOptions = app.get('authLocalOptions');

  local.options = {
    ...authLocalOptions,
    secret: "figVamVsemANeSecret", 
    Verifier: local.Verifier
  };


 /*
  local.options = {
       secret: 'figVamVsemANeSecret', // the secret for hashing password
       name: 'local', // the name to use when invoking the authentication Strategy
       entity: 'user', // the entity that you're comparing username/password against
       service: 'users', // the service to look up the entity
       usernameField: 'email', // key name of username field
       passwordField: 'password', // key name of password field
       entityUsernameField: 'email', // key name of the username field on the entity (defaults to `usernameField`) 
       entityPasswordField: 'password', // key name of the password on the entity (defaults to `passwordField`) 
       passReqToCallback: true, // whether the request object should be passed to `verify`
       session: false, // whether to use sessions,
       jwt: {
               secretOrPrivateKey: 'figVamVsemANeSecret', 
               header: { typ: 'access' }, // by default is an access token but can be any type. This is not a typo!
               audience: 'http://localhost:3030', // The resource server where the token is processed
               subject: 'anonymous', // Typically the entity id associated with the JWT
               issuer: 'feathers', // The issuing server, application or resource
               algorithm: 'HS256', // the algorithm to use
               expiresIn: '1d' // the access token expiry
           },
       Verifier: local.Verifier    
  }
*/