import test from 'ava';
import { assert } from 'chai';
import _ from 'lodash';
import supertest from 'supertest-promised';
import { generate } from 'randomstring';
import app from '../../../src/app';
import setMaxListener from '../../helpers/set-max-listener';

setMaxListener();
const request = supertest(app);

const locationGroupHeaders = {
  'x-location-group': generate(12)
};

const catalog = {
  name: 'catalog1',
  url: 'test.com',
  orderNumber: 1,
  startPage: 50,
  endPage: 51,
};

const mindmapIds = { publicMindmapList: [{ mindmapId: 'gfdfgshfdshf' }] };

const category = {
  name: 'category',
  description: 'Category description goes here',
  orderNumber: 10,
  invoiceProperty: 'prettyId',
  mindmapIds,
  productFieldSchema: {
    numberedField: {
      type: 'number'
    },
    textField: {
      type: 'text'
    },
    temperatureField: {
      type: 'temperature'
    },
    imageField: {
      type: 'image'
    },
    chooseOneField: {
      type: 'chooseOne',
      items: ['one', 'two', 'three']
    }
  }
};

const expectedProductSchema = {
  numberedField: {
    type: 'number'
  },
  textField: {
    type: 'string'
  },
  temperatureField: {
    type: 'number'
  },
  imageField: {
    type: 'string'
  },
  chooseOneField: {
    type: 'string'
  }
};

test.serial.before('create catalog', async () => {
  const response = await request
    .post('/catalogs')
    .set({
      ...locationGroupHeaders
    })
    .set('content-type', 'application/json')
    .send(catalog)
    .expect(201);

  catalog.id = response.body.id;
  category.catalogs = [{
    catalogId: catalog.id
  }];
  category.suppliers = [{
    catalogId: 'wefwef',
    name: 'suppliersCatalog1',
    primary: true,
    orderNumber: 0
  }];
});

test.serial.after('delete catalog', async () => {
  await request
    .delete(`/catalogs/${catalog.id}`)
    .set({
      ...locationGroupHeaders
    })
    .set('content-type', 'application/json')
    .send()
    .expect(200);
});

test.serial('create category', async () => {
  const response = await request
    .post('/categories')
    .set({
      ...locationGroupHeaders
    })
    .set('content-type', 'application/json')
    .send(category)
    .expect(201);

  assert.isDefined(response.body.id);
  category.id = response.body.id;
  category.productSchema = response.body.productSchema;
  _.forEach(category, (val, key) => { assert.include(_.keys(category), key); });
});

test.serial('productSchema should be as expected', () => {
  assert.deepEqual(category.productSchema, expectedProductSchema);
});

test.serial('do not allow create category without productSchema', async () => {
  await request
    .post('/categories')
    .set({
      ...locationGroupHeaders
    })
    .set('content-type', 'application/json')
    .send({
      name: 'category1'
    })
    .expect(400);
});

test.serial('do not allow create category with wrong productSchema', async () => {
  await request
    .post('/categories')
    .set({
      ...locationGroupHeaders
    })
    .set('content-type', 'application/json')
    .send({
      name: 'category1',
      productFieldSchema: {
        field: {
          type: 'evil'
        }
      }
    })
    .expect(400);
});

test.serial('update category', async () => {
  category.name = 'another name';

  const response = await request
    .put(`/categories/${category.id}`)
    .set({
      ...locationGroupHeaders
    })
    .set('content-type', 'application/json')
    .send(category)
    .expect(200);

  assert.equal(response.body.name, category.name);
});

test.serial('patch category', async () => {
  category.name = 'just one more name value';

  const response = await request
    .patch(`/categories/${category.id}`)
    .set({
      ...locationGroupHeaders
    })
    .set('content-type', 'application/json')
    .send({ name: category.name })
    .expect(200);

  assert.equal(response.body.name, category.name);
});

test.serial('get all categories', async () => {
  const response = await request
    .get('/categories')
    .set({
      ...locationGroupHeaders
    })
    .set('content-type', 'application/json')
    .send()
    .expect(200);

  assert.isAtLeast(response.body.length, 1);
});

test.serial('suppliers is defined', async () => {
  const response = await request
    .get(`/categories/${category.id}`)
    .set({
      ...locationGroupHeaders
    })
    .set('content-type', 'application/json')
    .send()
    .expect(200);

  assert.isDefined(response.body.suppliers);
});

test.serial('set supplier for category', async () => {

  const suppliers = [
    {
      catalogId: '123123qwe',
      name: 'suppliersCatalog1',
      primary: true,
      orderNumber: 0
    }
  ];

  const response = await request
    .patch(`/categories/${category.id}`)
    .set({
      ...locationGroupHeaders
    })
    .set('content-type', 'application/json')
    .send({ suppliers })
    .expect(200);
  assert.deepEqual(response.body.suppliers, suppliers);
});

test.serial('pagination headers exist', async () => {
  const response = await request
    .get('/categories')
    .set({
      ...locationGroupHeaders
    })
    .set('content-type', 'application/json')
    .send()
    .expect(200);

  assert.isDefined(response.headers['x-total-count']);
  assert.isDefined(response.headers['x-ratelimit-remaining']);
});

test.serial('pagination headers return appropriately', async () => {
  const response = await request
    .get('/categories')
    .set({
      ...locationGroupHeaders
    })
    .set('content-type', 'application/json')
    .query({ $limit: 1 })
    .send()
    .expect(200);

  const totalCount = _.toNumber(response.headers['x-total-count']);
  const remaining = _.toNumber(response.headers['x-ratelimit-remaining']);

  assert.isAtLeast(totalCount, 1);
  assert.isBelow(remaining, totalCount, 'x-ratelimit-remaining is less than x-total-count');
});

test.serial('get category', async () => {
  const response = await request
    .get(`/categories/${category.id}`)
    .set({
      ...locationGroupHeaders
    })
    .set('content-type', 'application/json')
    .send()
    .expect(200);

  assert.equal(response.body.id, category.id);
});

test.serial('get category, is defined mindmapIds', async () => {
  const response = await request
    .get(`/categories/${category.id}`)
    .set({
      ...locationGroupHeaders
    })
    .set('content-type', 'application/json')
    .send()
    .expect(200);

  assert.deepEqual(response.body.mindmapIds, category.mindmapIds);
});

test.serial('delete category', async () => {
  await request
    .delete(`/categories/${category.id}`)
    .set({
      ...locationGroupHeaders
    })
    .set('content-type', 'application/json')
    .send()
    .expect(200);
});
