module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const products = new Schema({
    name: { type: String, required: true, unique: true  },
    user: {type: mongooseClient.Schema.Types.ObjectId, ref: 'users'},
    category: {type: mongooseClient.Schema.Types.ObjectId, ref: 'categories'} 
  }, {
    timestamps: true
  });

  return mongooseClient.model('products', products);
};