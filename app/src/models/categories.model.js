// categories-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const categories = new Schema({
    name: { type: String, required: true, unique: true },
    user: {type: mongooseClient.Schema.Types.ObjectId, ref: 'users'}
  }, {
    timestamps: true
  });

  return mongooseClient.model('categories', categories);
};
