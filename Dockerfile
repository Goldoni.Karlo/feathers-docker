FROM node:8.11.4-slim
COPY ./app /app
RUN npm install
RUN groupadd -r uwsgi
RUN useradd -r -g uwsgi uwsgi
WORKDIR /app
EXPOSE 3030
USER uwsgi
CMD ["npm", "start"]